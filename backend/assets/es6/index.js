'use strict'

const run = (nim, password) => {
  let getHistory = () => {
    $.post(`${url}/gethistoryuser/${nim}`, {
      nim
    }, data => {
      if (data.length === 2) {
        alert('nim atau password salah')
        return false
      }
  
      $('.home').fadeOut(() => {
        $('.home').find('input').each((_, item) => {
          $(item).val('')
        })
  
        $('.dashboard').fadeIn(() => {
          $('.dashboard').html(`
            <table style="width: 100%">
              <thead>
                <th>Matakuliah</th>
                <th>SKS</th>
                <th>Grade</th>
                <th>Bobot</th>
                <th>tahun</th>
              </thead>
            </table>
          `)
  
          data_table(JSON.parse(data))
        })
      })
    })
  }

  $.post(`${url}/login/${nim}/${password}`, {
    nim, password
  }, data => {
    data = JSON.parse(data)

    if (typeof user === 'undefined') {
      window.user = {
        nim, password, picture : data.url_foto
      }
    }

    getHistory()
  })
}, data_table = (data) => {
  $('table').dataTable({
    aaData: data,
    ordering: false,
    responsive: true,
    dom: 'Bfrtip', 
    buttons: [ 'excel', 'pdf' ],
    aoColumns: [
      { mData: 'matakuliah'},
      { mData: 'sks' },
      { mData: 'grade' },
      { mData: 'bobot' },
      { mData: 'tahun' },
    ]
  })

  profile()
}, profile = () => {
  if (typeof user === 'undefined') return false

  $('.user-profile').css({
    background: `url(${user.picture})`
  })
  
  $('.user-profile').fadeIn()
}

$('.home').find('input[type=submit]').on('click', () => {
  let nim = $('.home').find('input[name=nim]').val(), password = $('.home').find('input[name=password]').val()

  if (nim === '' || password === '') return false

  run(nim, password)
})

$('.user-profile').on('click', () => {
  $.get(`${url}/logout/`, () => {
    window.location.reload()
  })
})