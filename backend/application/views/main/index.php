<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Scrape Web Apps</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url("assets/css/normalize.css") ?>">
	<link rel="stylesheet" href="<?= base_url("assets/css/skeleton.css") ?>">
	<link rel="stylesheet/less" href="<?= base_url("assets/less/app.less") ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url("assets/DataTables/datatables.min.css") ?>"/>
	<script src="<?= base_url("assets/js/less.min.js") ?>"></script>
	<script src="<?= base_url("assets/js/babel.min.js") ?>"></script>
	<script src="<?= base_url("assets/js/jquery.js") ?>"></script>
	<script src="<?= base_url("assets/DataTables/datatables.min.js") ?>"></script>
	<script src="<?= base_url("assets/DataTables/pdfmake-0.1.32/vfs_fonts.js") ?>" async="true" defer="true"></script>
	<script src="<?= base_url("assets/es6/index.js") ?>" async="true" defer="true" type="text/babel"></script>
</head>
<body>
	<div class="home">
		<div class="row">
			<div class="six columns"><label for="">NIM</label><input type="text" class="u-full-width" name="nim"></div>
			<div class="six columns"><label for="">Password</label><input type="password" class="u-full-width" name="password"></div>
			<input type="submit" class="button-primary" value="Login">
		</div>
	</div>
	<div class="dashboard" style="display: none">
	</div>
	<div class="user-profile" style="display: none">
	</div>
	<script>
		window.url = "<?= site_url("scrape") ?>"
	</script>
</body>
</html>